package org.acme;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DummyFactoryTest {
    

    @Test
    public void test() {
        assertEquals(50, DummyFactory.multipleByFive(10));
    }

}