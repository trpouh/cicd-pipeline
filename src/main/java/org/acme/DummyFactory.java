package org.acme;

public class DummyFactory {
    
    public static int multipleByFive(int input) {
        return input * 5;
    }

}
